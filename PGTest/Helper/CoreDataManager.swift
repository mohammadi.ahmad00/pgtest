//
//  CodeDataManager.swift
//  PGTest
//
//  Created by Ahmad on 11/30/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataManager {
    
    
    private init() {
        
    }
    
    static let shared = CoreDataManager()

    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "PGTest")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context = persistentContainer.viewContext
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
