//
//  Constants.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class Constants {
    
    static let ApiKey = "xqgWj2GNs4EENKAZzcCV7iHhcyeTAsIg"
    static let connectionFailedError = "No Internet Connection"
    static let baseImageUrl = "https://static01.nyt.com/"
    static let baseTopStoryUrl = "https://api.nytimes.com/svc/topstories/v2/science.json"
    static let baseSearchUrl = "https://api.nytimes.com/svc/search/v2/articlesearch.json"
}
