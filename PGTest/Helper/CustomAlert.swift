//
//  CustomAlert.swift
//  PGTest
//
//  Created by Ahmad on 11/30/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class CustomAlert {
    
    enum AlertType {
        case Error
        case Warning
        case Success
    }
    
    private static let _shared = CustomAlert()
    
    static var Shared : CustomAlert {
        return _shared
    }
    
    private var keyWindow : UIWindow!
    
    private var autoHide = true
    
    private var isAlertHidden = true
    
    private var containerView : UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        view.layer.cornerRadius = 12
        return view
    }()
    
    private var dummyBackView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        return view
    }()
    
    private var alertLbl: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.adjustsFontSizeToFitWidth = true
        lbl.font = lbl.font.withSize(15)
        lbl.alpha = 0
        return lbl
    }()
    
    private let alertHeight: CGFloat = 50
    
    private var timer : Timer!
    
    func showAlert(message: String, autoHide: Bool = true, type: AlertType) {
        
        switch type {
        case .Error:
            dummyBackView.backgroundColor = UIColor(red: 249/255, green: 66/255, blue: 47/255, alpha: 1)
            break
        case .Warning:
            dummyBackView.backgroundColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)
            break
        case .Success:
            dummyBackView.backgroundColor = UIColor(red: 97/255, green: 161/255, blue: 23/255, alpha: 1)
            break
        }
        
        
        if (!isAlertHidden) {
            if (autoHide){
                timer.invalidate()
            }
            alertLbl.text = message
            UIView.animate(withDuration: 0.09, delay: 0, options: .curveEaseOut, animations: {
                
                self.containerView.transform = CGAffineTransform(scaleX: 1.12, y: 1.12)
                
            }) { (done) in
                
                UIView.animate(withDuration: 0.09, delay: 0, options: .curveEaseIn, animations: {
                    
                    self.containerView.transform = CGAffineTransform.identity
                    
                }, completion: { (done) in
                    
                    if (autoHide) {
                        self.timer.fire()
                    }
                    
                })
                
            }
            
            return
        }
        
        self.autoHide = autoHide
        
        keyWindow = UIApplication.shared.keyWindow
        
        if (UIDevice.current.userInterfaceIdiom == .phone) {
            containerView.frame = CGRect(x: keyWindow.frame.width * 0.05, y: 50, width: keyWindow.frame.width * 0.9, height: alertHeight)
        }else if (UIDevice.current.userInterfaceIdiom == .pad) {
            containerView.frame = CGRect(x: keyWindow.frame.width * 0.2, y: 50, width: keyWindow.frame.width * 0.6, height: alertHeight)
        }
        keyWindow.addSubview(containerView)
        
        dummyBackView.frame = CGRect(x: (containerView.frame.width - alertHeight) / 2, y: 0, width: alertHeight, height: alertHeight)
        dummyBackView.layer.cornerRadius = dummyBackView.frame.width / 2
        dummyBackView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        containerView.addSubview(dummyBackView)
        
        alertLbl.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height)
        alertLbl.text = message
        containerView.addSubview(alertLbl)
        
        UIView.animate(withDuration: 0.6, delay: 0.1, options: .curveEaseInOut, animations: {
            self.dummyBackView.transform = CGAffineTransform(scaleX: 10, y: 10)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.3, options: [], animations: {
            self.alertLbl.alpha = 1
        }, completion: nil)
        
        
        isAlertHidden = false
        
        if autoHide {
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(hideAlertAuto), userInfo: nil, repeats: false)
        }
        
    }
    
    @objc private func hideAlertAuto() {
        
        isAlertHidden = true
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.alertLbl.alpha = 0
            
        }) { (done) in
            
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseIn, animations: {
            
            self.dummyBackView.transform = CGAffineTransform.identity
            
        }, completion: { (done) in
            
            self.dummyBackView.removeFromSuperview()
            self.alertLbl.removeFromSuperview()
            self.containerView.removeFromSuperview()
            
        })
        
    }
    
    
    func hideAlert() {
        
        if autoHide {
            return
        }
        
        isAlertHidden = true
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.alertLbl.alpha = 0
            
        }) { (done) in
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
                
                self.dummyBackView.transform = CGAffineTransform.identity
                
            }, completion: { (done) in
                
                self.dummyBackView.removeFromSuperview()
                self.alertLbl.removeFromSuperview()
                self.containerView.removeFromSuperview()
                
            })
        }
        
    }
    
    
}

