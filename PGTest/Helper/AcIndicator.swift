//
//  AcIndicator.swift
//  PGTest
//
//  Created by Ahmad on 12/2/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

extension UIActivityIndicatorView {
    
    func showCustomIndicator(parentVc: UIViewController) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.color = .red
        self.hidesWhenStopped = true
        
        parentVc.view.addSubview(self)
        NSLayoutConstraint.activate([self.centerXAnchor.constraint(equalTo: parentVc.view.centerXAnchor),
                                     self.centerYAnchor.constraint(equalTo: parentVc.view.centerYAnchor),
                                     self.widthAnchor.constraint(equalToConstant: 50),
                                     self.heightAnchor.constraint(equalToConstant: 50)
            ])
        parentVc.view.bringSubviewToFront(self)
        self.startAnimating()
        
    }
    
    func hideCustomIndicator() {
        
        self.stopAnimating()
        self.removeFromSuperview()
        
    }
    
    
}
