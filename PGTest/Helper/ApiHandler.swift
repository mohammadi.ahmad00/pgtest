//
//  ApiHandler.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

typealias StoriesResponse = (_ stories: [Story]?, _ error: String?) -> Void

class ApiHandler {
    
    
    
    private static var searchTask : URLSessionDataTask?
    public static func getNews(forSearchString searchString: String, page: Int, result: @escaping StoriesResponse) {
        
        do {
            
            var requestUrlComp = URLComponents(string: Constants.baseSearchUrl)
            
            requestUrlComp?.queryItems = [
                URLQueryItem(name: "q", value: searchString),
                URLQueryItem(name: "api-key", value: Constants.ApiKey),
                URLQueryItem(name: "page", value: "\(page)")
            ]
            
            guard let url = requestUrlComp?.url else {
                result(nil, Constants.connectionFailedError)
                return
            }
            
            let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10.0)
            request.httpMethod = "GET"
            
            searchTask?.cancel()
            DispatchQueue.global(qos: .userInteractive).async {
                
                searchTask = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        
                        if (error != nil) {
                            result(nil, Constants.connectionFailedError)
                            return
                        }
                        
                        if ((response as! HTTPURLResponse).statusCode != 200) {
                            result(nil, Constants.connectionFailedError)
                            return
                        }
                        
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! Dictionary<String, AnyObject>
                            
                            let res = json["response"] as! Dictionary<String, AnyObject>
                            let docs = res["docs"] as! Array<Dictionary<String, AnyObject>>
                            
                            var storiesArray = [Story]()
                            for doc in docs {
                                let story = Story()
                                story.abstract = doc["abstract"] as! String
                                let headLine = doc["headline"] as! Dictionary<String, AnyObject>
                                story.headline = headLine["main"] as! String
                                story.publishDate = doc["pub_date"] as! String
                                story.url = doc["web_url"] as! String
                                let multimedia = doc["multimedia"] as! Array<Dictionary<String, AnyObject>>
                                if let media = multimedia.first {
                                    story.imageUrl = media["url"] as! String
                                }
                                storiesArray.append(story)
                            }
                            
                            result(storiesArray, nil)
                            return
                            
                        } catch {
                            result(nil, Constants.connectionFailedError)
                            return
                            
                        }
                        
                    }
                    
                })
                
                searchTask!.resume()
                
            }
            
        }
    }
    static func cancelSearchTask() {
        
        if let task = searchTask {
            task.cancel()
        }
        
    }
    
    
    private static var topStoryTask : URLSessionDataTask?
    public static func getTopSoties(result: @escaping StoriesResponse) {
        
        do {
            
            var requestUrlComp = URLComponents(string: Constants.baseTopStoryUrl)
            
            requestUrlComp?.queryItems = [
                URLQueryItem(name: "api-key", value: Constants.ApiKey)
            ]
            
            guard let url = requestUrlComp?.url else {
                result(nil, Constants.connectionFailedError)
                return
            }
            
            let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10.0)
            request.httpMethod = "GET"
            
            searchTask?.cancel()
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                searchTask = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        
                        if (error != nil) {
                            result(nil, Constants.connectionFailedError)
                            return
                        }
                        
                        if ((response as! HTTPURLResponse).statusCode != 200) {
                            result(nil, Constants.connectionFailedError)
                            return
                        }
                        
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! Dictionary<String, AnyObject>
                            
                            let results = json["results"] as! Array<Dictionary<String, AnyObject>>
                            
                            var storiesArray = [Story]()
                            for res in results {
                                let story = Story()
                                story.headline = res["title"] as! String
                                story.abstract = res["abstract"] as! String
                                story.publishDate = res["published_date"] as! String
                                story.url = res["url"] as! String
                                let multimedia = res["multimedia"] as! Array<Dictionary<String, AnyObject>>
                                if let media = multimedia.first {
                                    story.imageUrl = media["url"] as! String
                                }
                                storiesArray.append(story)
                            }
                            
                            result(storiesArray, nil)
                            return
                            
                        } catch {
                            result(nil, Constants.connectionFailedError)
                            return
                            
                        }
                        
                    }
                    
                })
                
                searchTask!.resume()
            }
            
        }
    }
    static func canceltopStoryTask() {
        
        if let task = searchTask {
            task.cancel()
        }
        
    }
    
}
