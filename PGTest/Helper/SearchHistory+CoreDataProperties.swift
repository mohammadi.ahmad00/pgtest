//
//  SearchHistory+CoreDataProperties.swift
//  PGTest
//
//  Created by Ahmad on 11/30/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//
//

import Foundation
import CoreData


extension SearchHistory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SearchHistory> {
        return NSFetchRequest<SearchHistory>(entityName: "SearchHistory")
    }

    @NSManaged public var submit_date: Int64
    @NSManaged public var search_item: String?

}
