//
//  StoryDetailVC.swift
//  PGTest
//
//  Created by Ahmad on 12/1/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit
import WebKit

class StoryDetailVC: UIViewController {
    
    let webView = WKWebView()
    
    private let loadingView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.red.withAlphaComponent(0.8)
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var loadingViewRightConstraint: NSLayoutConstraint!
    
    private var url : URL
    
    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Details ..."
        view.backgroundColor = .white
        edgesForExtendedLayout = []
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setupView() {
        
        webView.accessibilityIdentifier = "detailWebView"
        view.addSubview(webView)
        view.addConstraintsWithFormat("H:|-(0)-[v0]-(0)-|", views: webView)
        view.addConstraintsWithFormat("V:|-(0)-[v0]-(0)-|", views: webView)
        
        let request = URLRequest(url: url)
        webView.load(request)
        
        view.addSubview(loadingView)
        
        NSLayoutConstraint.activate([loadingView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                     loadingView.topAnchor.constraint(equalTo: view.topAnchor),
                                     loadingView.heightAnchor.constraint(equalToConstant: 2)
            ])
        
        loadingViewRightConstraint = loadingView.rightAnchor.constraint(equalTo: view.leftAnchor)
        loadingViewRightConstraint.isActive = true
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
    }
    
    deinit {
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            
            
            if webView.estimatedProgress != 1 && loadingView.alpha != 0.8 {
                loadingView.alpha = 0.8
            }
            
            
            loadingViewRightConstraint.constant = (view.frame.width) * CGFloat(webView.estimatedProgress)
            
            UIView.animate(withDuration: 0.06, delay: 0, options: .curveEaseOut, animations: {[weak self] in
                guard let sSelf = self else {
                    return
                }
                
                sSelf.view.layoutIfNeeded()
                
            }) { [weak self] (done) in
                guard let sSelf = self else {
                    return
                }
                if sSelf.webView.estimatedProgress == 1 {
                    sSelf.loadingView.alpha = 0
                    sSelf.loadingViewRightConstraint.constant = 0
                }
            }
            
        }
        
    }
    
}

