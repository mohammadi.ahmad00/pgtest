//
//  IntroVC.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {
    
    private let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = .black
        lbl.text = "Property Guru Test"
        lbl.font = UIFont.systemFont(ofSize: 26, weight: .bold)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let subtitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = .black
        lbl.text = "By Ahmad Mohammadi"
        lbl.font = UIFont.systemFont(ofSize: 17, weight: .light)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupFakeTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setupView() {
        
        view.backgroundColor = .white
        
        view.addSubview(titleLbl)
        view.addSubview(subtitleLbl)
        
        NSLayoutConstraint.activate([titleLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     titleLbl.bottomAnchor.constraint(equalTo: view.centerYAnchor),
                                     titleLbl.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
                                     titleLbl.heightAnchor.constraint(lessThanOrEqualToConstant: 100)
            ])
        
        NSLayoutConstraint.activate([subtitleLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                     subtitleLbl.topAnchor.constraint(equalTo: view.centerYAnchor, constant: 8),
                                     subtitleLbl.widthAnchor.constraint(equalTo: titleLbl.widthAnchor),
                                     subtitleLbl.heightAnchor.constraint(lessThanOrEqualToConstant: 100)
            ])
        
        
    }
    
    
    private func setupFakeTimer() {
        
        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(timerAction), userInfo: nil, repeats: false)
        
    }
    
    @objc private func timerAction() {
        self.navigationController?.show(MainVC(coreDataManager: CoreDataManager.shared), sender: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.removeFromParent()
    }
    
}
