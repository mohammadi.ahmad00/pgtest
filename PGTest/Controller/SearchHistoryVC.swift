//
//  SearchHistoryVC.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

protocol SearchHistoryDelegate {
    func didSelectSearchHistoryItem(string: String)
}

class SearchHistoryVC: UIViewController {
    
    var delegate: SearchHistoryDelegate?
    
    private var historyItem = [SearchHistory]() {
        didSet {
            historyTv.reloadData()
        }
    }
    
    private let historyTvCellIdentifier = "historyTvCellIdentifier"
    let historyTv: UITableView = {
        let tbl = UITableView(frame: .zero, style: .plain)
        tbl.accessibilityIdentifier = "historyTv"
        tbl.rowHeight = 50
        return tbl
    }()
    
    private let coreDataManager: CoreDataManager
    
    init(coreDataManager: CoreDataManager) {
        self.coreDataManager = coreDataManager
        super.init(nibName: nil, bundle: nil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        retriveSavedData()
    }
    
    private func setupView() {
        
        historyTv.delegate = self
        historyTv.dataSource = self
        historyTv.register(UITableViewCell.self, forCellReuseIdentifier: historyTvCellIdentifier)
        view.addSubview(historyTv)
        view.addConstraintsWithFormat("H:|-(0)-[v0]-(0)-|", views: historyTv)
        view.addConstraintsWithFormat("V:|-(0)-[v0]-(0)-|", views: historyTv)
        
    }
    
}


//MARK: - UITableDelegate and UITableDataSource
extension SearchHistoryVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: historyTvCellIdentifier, for: indexPath)
        cell.textLabel?.text = historyItem[indexPath.row].search_item ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectSearchHistoryItem(string: historyItem[indexPath.row].search_item ?? "")
    }
    
}

//MARK: - Fetch Data
extension SearchHistoryVC {
    
    private func retriveSavedData() {
        
        var items : [SearchHistory]?
        
        do {
            items = try (coreDataManager.context.fetch(SearchHistory.fetchRequest()) as? [SearchHistory])
        } catch {
            
        }
        
        if items == nil {
            return
        }
        
        items!.sort { (s0, s1) -> Bool in
            return s0.submit_date > s1.submit_date
        }
        
        if items!.count > 10 {
            coreDataManager.context.delete(items!.last!)
        }
        
        historyItem = items!
        
    }
    
}
