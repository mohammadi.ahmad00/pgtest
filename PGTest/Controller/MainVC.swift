//
//  MainVC.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UISearchBarDelegate, SearchHistoryDelegate {
    
    private var stories = [StoryViewModel]()
    
    private let storyCollectionCellIndentifier = "stroryCollectionCellIndentifier"
    let storyCv: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 30
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.accessibilityIdentifier = "storyCv"
        cv.backgroundColor = .clear
        return cv
    }()
    
    private let searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.searchBarStyle = UISearchBar.Style.prominent
        sb.placeholder = " Search..."
        sb.sizeToFit()
        sb.isTranslucent = false
        sb.returnKeyType = UIReturnKeyType.search
        return sb
    }()
    
    private let acIndicator = UIActivityIndicatorView(style: .white)
    
    private var refreshControl = UIRefreshControl()
    
    private let coreDataManager: CoreDataManager
    
    init(coreDataManager: CoreDataManager, storyVm:StoryViewModel? = nil) {
        self.coreDataManager = coreDataManager
        super.init(nibName: nil, bundle: nil)
        #if DEBUG
        
        let st = Story()
        st.headline = "Headline"
        st.abstract = "Abstract"
        st.publishDate = "2019-12-01T02:37:15-05:00"
        st.url = "https://www.google.com/"
        st.imageUrl = Constants.baseImageUrl + "dummy_img_url.jpg"
        
        for _ in 0...9 {
            self.stories.append(storyVm ?? StoryViewModel(story: st))
        }
        #endif
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.titleView = searchBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
        setupView()
        
        #if DEBUG
            storyCv.reloadData()
        #else
            getData()
        #endif
        
    }
    
    private func setupView() {
        
        view.backgroundColor = .white
        
        storyCv.delegate = self
        storyCv.dataSource = self
        storyCv.register(StoryCollectionCell.self, forCellWithReuseIdentifier: storyCollectionCellIndentifier)
        view.addSubview(storyCv)
        view.addConstraintsWithFormat("H:|-(0)-[v0]-(0)-|", views: storyCv)
        view.addConstraintsWithFormat("V:|-(0)-[v0]-(0)-|", views: storyCv)
        
        searchBar.delegate = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        storyCv.refreshControl = refreshControl
        
    }
    
    private var isFetching = false
    private var page: Int = 1
    private var isTopStory = true
    private var searchString = ""
    
    private var prevOffset: CGFloat = 0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if isTopStory {
            return
        }
        
        let currentOffset = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if !isFetching && (currentOffset > contentHeight - (scrollView.frame.height * 1.5)) {
            getData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let sText = searchBar.text else {
            return
        }
        
        if sText == "" {
            return
        }
        
        stories.removeAll()
        storyCv.reloadData()
        dismissSearchHistoryView()
        saveToSearchHistory(searchString: sText)
        
        searchString = sText
        isTopStory = false
        page = 1
        getData()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        showSearchHistoryView()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        dismissSearchHistoryView()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        dismissSearchHistoryView()
    }
    
    func didSelectSearchHistoryItem(string: String) {
        searchBar.text = string
        
        stories.removeAll()
        storyCv.reloadData()
        dismissSearchHistoryView()
        
        searchString = string
        isTopStory = false
        page = 1
        getData()
    }
    
    func saveToSearchHistory(searchString: String) {
        
        let sHistory = SearchHistory(context: coreDataManager.context)
        sHistory.search_item = searchString
        sHistory.submit_date = Int64(Date().timeIntervalSince1970)
        coreDataManager.saveContext()
        
    }
    
    private func showSearchHistoryView() {
        
        let historyVC = SearchHistoryVC(coreDataManager: CoreDataManager.shared)
        historyVC.delegate = self
        self.addChild(historyVC)
        view.addSubview(historyVC.view)
        view.addConstraintsWithFormat("H:|-(0)-[v0]-(0)-|", views: historyVC.view)
        view.addConstraintsWithFormat("V:|-(0)-[v0]-(0)-|", views: historyVC.view)
        historyVC.didMove(toParent: self)
        
    }
    
    private func dismissSearchHistoryView() {
        if children.count > 0 {
            for child in children {
                if ((child as? SearchHistoryVC) != nil) {
                    child.willMove(toParent: nil)
                    child.view.removeFromSuperview()
                    child.removeFromParent()
                    break
                }
            }
        }
        searchBar.endEditing(true)
    }
    
    @objc private func refresh() {
        page = 1
        getData()
    }
    
}

//MARK: - UICollectionDelegate and UICollectionDataSource
extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: storyCollectionCellIndentifier, for: indexPath) as! StoryCollectionCell
        if !refreshControl.isRefreshing {
            cell.storyVM = stories[indexPath.row]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.width)
        }else {
            return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.width/2)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.show(StoryDetailVC(url: stories[indexPath.row].detailUrl), sender: nil)
    }
    
}


//MARK: - Fetch Data
extension MainVC {
    
    private func getData() {
        
        isFetching = true
        
        if isTopStory {
            
            acIndicator.showCustomIndicator(parentVc: self)
            stories.removeAll()
            ApiHandler.getTopSoties {[weak self] (storiesArray, error) in
                
                guard let sSelf = self else {
                    return
                }
                sSelf.acIndicator.hideCustomIndicator()
                if sSelf.refreshControl.isRefreshing {
                    sSelf.refreshControl.endRefreshing()
                }
                sSelf.isFetching = false
                
                if error != nil {
                    CustomAlert.Shared.showAlert(message: error!, type: .Error)
                    return
                }
                
                sSelf.stories = storiesArray!.map({ (st) -> StoryViewModel in
                    return StoryViewModel(story: st)
                })
                sSelf.storyCv.reloadData()
            }
            
        }else {
            
            if page == 1 {
                acIndicator.showCustomIndicator(parentVc: self)
                stories.removeAll()
            }
            
            ApiHandler.getNews(forSearchString: searchString, page: page) {[weak self] (storiesArray, error) in
                
                guard let sSelf = self else {
                    return
                }
                sSelf.acIndicator.hideCustomIndicator()
                if sSelf.refreshControl.isRefreshing {
                    sSelf.refreshControl.endRefreshing()
                }
                sSelf.isFetching = false
                
                if error != nil {
                    CustomAlert.Shared.showAlert(message: error!, type: .Error)
                    return
                }
                
                for st in storiesArray! {
                    sSelf.stories.append(StoryViewModel(story: st))
                }
                
                sSelf.storyCv.reloadData()
                sSelf.page += 1
                
            }
            
            
        }
        
    }
    
}
