//
//  StoryViewModel.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class StoryViewModel {
    
    var title = String()
    var date = String()
    var abstract = String()
    var imageUrl : URL!
    var detailUrl : URL!
    
    init(story: Story) {
        self.title = story.headline
        self.abstract = story.abstract
        self.date = processDate(rawDate: story.publishDate)
        self.detailUrl = URL(string: story.url)
        if story.imageUrl != "" {
            
            if story.imageUrl.contains(Constants.baseImageUrl) {
                self.imageUrl = URL(string: story.imageUrl)
            }else {
                self.imageUrl = URL(string: Constants.baseImageUrl + story.imageUrl)
            }
        }else {
            self.imageUrl = URL(string: "https://static01.nyt.com/newsgraphics/images/icons/defaultPromoCrop.png")
        }
    }
    
    
    private func processDate(rawDate: String) -> String {
        let st = rawDate.split(separator: "T")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateOp = formatter.date(from: String(st[0]))
        formatter.dateFormat = "dd LLLL yyyy"
        return formatter.string(from: dateOp!)
    }
    
}



