//
//  Story.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class Story {
    var headline = String()
    var abstract = String()
    var publishDate = String()
    var url = String()
    var imageUrl = String()
}
