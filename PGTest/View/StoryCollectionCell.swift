//
//  StoryCollectionCell.swift
//  PGTest
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import UIKit

class StoryCollectionCell: UICollectionViewCell {
    
    private let iv: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        return iv
    }()
    
    private let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let dateLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = .darkGray
        lbl.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let summeryLbl: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = .darkGray
        lbl.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var storyVM: StoryViewModel! {
        didSet {
            if let story = self.storyVM {
                iv.loadFromUrl(story.imageUrl)
                titleLbl.text = story.title
                dateLbl.text = story.date
                summeryLbl.text = story.abstract
            }
        }
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        
        addSubview(iv)
        addSubview(titleLbl)
        addSubview(dateLbl)
        addSubview(summeryLbl)
        
        
        NSLayoutConstraint.activate([iv.topAnchor.constraint(equalTo: topAnchor),
                                     iv.centerXAnchor.constraint(equalTo: centerXAnchor),
                                     iv.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
                                     iv.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.65)
            ])
        
        NSLayoutConstraint.activate([titleLbl.leftAnchor.constraint(equalTo: iv.leftAnchor,constant: 4),
                                     titleLbl.rightAnchor.constraint(equalTo: iv.rightAnchor, constant: -4),
                                     titleLbl.topAnchor.constraint(equalTo: iv.bottomAnchor, constant: 6),
                                     titleLbl.heightAnchor.constraint(lessThanOrEqualToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([dateLbl.leftAnchor.constraint(equalTo: titleLbl.leftAnchor),
                                     dateLbl.rightAnchor.constraint(equalTo: titleLbl.rightAnchor),
                                     dateLbl.topAnchor.constraint(equalTo: titleLbl.bottomAnchor),
                                     dateLbl.heightAnchor.constraint(lessThanOrEqualToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([summeryLbl.leftAnchor.constraint(equalTo: titleLbl.leftAnchor),
                                     summeryLbl.rightAnchor.constraint(equalTo: titleLbl.rightAnchor),
                                     summeryLbl.topAnchor.constraint(equalTo: dateLbl.bottomAnchor),
                                     summeryLbl.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        
    }
    
}
