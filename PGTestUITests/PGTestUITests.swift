//
//  PGTestUITests.swift
//  PGTestUITests
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import XCTest

class PGTestUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNavigation() {
        
        let app = XCUIApplication()
//        let collectionView = app.otherElements.containing(.navigationBar, identifier:"PGTest.MainVC")
        let collectionView = app.collectionViews
        
        let storyCv = collectionView["storyCv"]
        let exists = NSPredicate(format: "exists == 1")
        
        expectation(for: exists, evaluatedWith: storyCv, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        
        XCTAssertTrue(storyCv.cells.count>0, "Collection View data not loaded")
        
        storyCv.cells.element(boundBy: 0).tap()
        
        let detailWebView = app.otherElements["detailWebView"]
        
        expectation(for: exists, evaluatedWith: detailWebView, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(detailWebView, "Detail Web view not Loaded")
        
    }
    
    
    func testSearch() {
        let app = XCUIApplication()
        let searchSearchField = app.navigationBars["PGTest.MainVC"].searchFields[" Search..."]
        
        let exists = NSPredicate(format: "exists == 1")
        
        expectation(for: exists, evaluatedWith: searchSearchField, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        searchSearchField.tap()
        let historyTv = app.tables["historyTv"]
        
        XCTAssertNotNil(historyTv, "History Table not Loaded")
        
        XCTAssert(app.keyboards.count > 0, "The keyboard is not shown")
        XCTAssert(app.keyboards.buttons["Search"].exists, "The keyboard has no Search button")
        
        
        searchSearchField.typeText("america")
        app.keyboards.buttons["Search"].tap()
        
        let notExists = NSPredicate(format: "exists != 1")
        
        expectation(for: notExists, evaluatedWith: historyTv, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(app.keyboards.count == 0, "The keyboard is not hidden")
        
        
        
        
        
        
        
    }
    

}
