//
//  PGTestTests.swift
//  PGTestTests
//
//  Created by Ahmad on 11/29/19.
//  Copyright © 2019 PropertyGuru. All rights reserved.
//

import XCTest
import CoreData
@testable import PGTest

class PGTestTests: XCTestCase {

    
    private var context : NSManagedObjectContext!
    private let searchStringTitle = "searchString"

    override func setUp() {

        continueAfterFailure = false

    }

    override func tearDown() {

    }

    func testTopStoriesApiCalls() {

        let exp = expectation(description: "top-stories-api-call")

        ApiHandler.getTopSoties { (stArray, error) in
            XCTAssertNil(error, "Top Stories api error: \(error!)")
            XCTAssertNotNil(stArray)

            if stArray!.count != 0 {
                let story = stArray!.first!
                XCTAssertNotEqual(story.headline, "", "Headline not set")
                XCTAssertNotEqual(story.abstract, "", "Abstract not set")
                XCTAssertNotEqual(story.publishDate, "", "Publish date not set")
                XCTAssertNotEqual(story.url, "", "Url not set")
            }else {
                XCTAssertTrue(false, "No Result Found for Search String")
            }

            exp.fulfill()
        }

        waitForExpectations(timeout: 10) { (err) in
            XCTAssertNil(err, "Request Time out error: \(err!.localizedDescription)")
        }

    }



    func testSearchApiCalls() {

        let exp2 = expectation(description: "top-stories-api-call")

        ApiHandler.getNews(forSearchString: "america", page: 1) { (stArray, error) in

            XCTAssertNil(error, "Top Stories api error: \(error!)")
            XCTAssertNotNil(stArray)

            if stArray!.count != 0 {
                let story = stArray!.first!
                XCTAssertNotEqual(story.headline, "", "Headline not set")
                XCTAssertNotEqual(story.abstract, "", "Abstract not set")
                XCTAssertNotEqual(story.publishDate, "", "Publish date not set")
                XCTAssertNotEqual(story.url, "", "Url not set")
            }else {
                XCTAssertTrue(false, "No Result Found for Search String")
            }

            exp2.fulfill()
        }

        waitForExpectations(timeout: 10) { (err) in
            XCTAssertNil(err, "Request Time out error: \(err!.localizedDescription)")
        }

    }


    func testViewModel() {
        let st = Story()
        st.headline = "Headline"
        st.abstract = "Abstract"
        st.publishDate = "2019-12-01T02:37:15-05:00"
        st.url = "https://www.google.com"
        st.imageUrl = Constants.baseImageUrl + "dummy_img_url.jpg"

        let stVM = StoryViewModel(story: st)

        XCTAssertEqual(stVM.title, st.headline, "Headlines must be same")
        XCTAssertEqual(stVM.abstract, st.abstract, "Abstract must be same")
        XCTAssertEqual(stVM.date, "01 December 2019", "Date processor not working correctly")
        XCTAssertEqual(stVM.detailUrl, URL(string: st.url), "Something's wrong with the detail Url")
        XCTAssertNotNil(URL(string: st.imageUrl), "Problem with image Url")
        XCTAssertEqual(stVM.imageUrl, URL(string: st.imageUrl), "Something's wrong with the detail Url")

        st.imageUrl = "images/icons/defaultPromoCrop.png"
        let stVM2 = StoryViewModel(story: st)
        XCTAssertEqual(stVM2.imageUrl, URL(string: Constants.baseImageUrl + st.imageUrl), "Something's wrong with the detail Url")

        st.imageUrl = ""
        let stVM3 = StoryViewModel(story: st)
        XCTAssertEqual(stVM3.imageUrl, URL(string: "https://static01.nyt.com/newsgraphics/images/icons/defaultPromoCrop.png"), "Something's wrong with the detail Url")

    }


    func testMainCollection() {

        let st = Story()
        st.headline = "Headline"
        st.abstract = "Abstract"
        st.publishDate = "2019-12-01T02:37:15-05:00"
        st.url = "https://www.google.com/"
        st.imageUrl = Constants.baseImageUrl + "dummy_img_url.jpg"
        
        let mainVC = MainVC(coreDataManager: CoreDataManager.shared, storyVm: StoryViewModel(story: st))
        _ = mainVC.view
        
        let collection = mainVC.storyCv
        
        XCTAssertTrue(collection.delegate is MainVC, "Collection view delegate not set")
        XCTAssertTrue(collection.dataSource is MainVC, "Collection view dataSource not set")
        
        XCTAssertTrue(collection.numberOfItems(inSection: 0)>0, "Collection has no Item!!!")
        
        
    }

    func testSearchHistoryTable() {

        let historyVC = SearchHistoryVC(coreDataManager: CoreDataManager.shared)
        _ = historyVC.view
        let tbl = historyVC.historyTv

        XCTAssertTrue(tbl.delegate is SearchHistoryVC, "Table view delegate not set")
        XCTAssertTrue(tbl.dataSource is SearchHistoryVC, "Table view dataSource not set")

    }

    func testDetailView() {

        let url = URL(string: "https://www.google.com/")!

        let detailVc = StoryDetailVC(url: url)
        detailVc.loadView()
        detailVc.viewDidLoad()

        XCTAssertNotNil(detailVc.webView.url, "Webview url not set")
        XCTAssertEqual(detailVc.webView.url!, url, "Url not set correctly")

    }

}
